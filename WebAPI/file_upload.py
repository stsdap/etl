from flask import Flask, render_template, request, jsonify
from werkzeug import secure_filename
import io
import os
from google.cloud import vision 
from google.cloud.vision import types
import json
import re
import csv
import pandas as pd 

    # Setting Envirronment Variable for Google Cloud Platform (Vision)
os.environ["GOOGLE_APPLICATION_CREDENTIALS"]="/users/pardeeprassani/Documents/HandwrittenTextDetection/cosmic-tenure-233207-dff701184033.json"

os.environ["FLASK_ENV"] ="development"
os.environ["FLASK_APP"] = "file_upload.py"


    # Method for detecting and extracting text from an image...
def detect_document(path):
    """Detects document features in an image."""

    client = vision.ImageAnnotatorClient()

    with io.open(path, 'rb') as image_file:
        content = image_file.read()

    image = vision.types.Image(content=content)

    response = client.document_text_detection(image=image)
    
    text = response.full_text_annotation.text
    text = text.casefold()                # Casefold is used for converting text in lowercase for easiness..
    text = replace_characters(text)       # Calling defined method to replace special charachters...
    return text

def replace_characters(text):             # A defined method for replacing special characters
    text = text.replace('(','')
    text = text.replace(')','')
    text = text.replace(':','')
    text = text.replace('.','')
    text = text.replace('"','')
    text = text.replace('  ', ' ')
    text = text.replace('_','')
    text = text.replace('-', '')
    text = text.replace(',','')
    text = text.replace("'",'')
    text = text.replace("`", "")
    text = text.replace('!', '')
    return text

def do_proper(text):                     # An other defined method for the proper format of the data..
    text = text.replace("\n", " ")       # Replaces newline with whitespace
    text = text.title()                  # Convert the data into the title case
    text = text.strip()                  # Removes extra whitespaces
    replace_characters(text)             # Calling method 
    return text


def search_name(text):                   # Method for Extracting Candiate Name.
    name=''
        # Possible Patterns for Extracting Candiate Name
    pattern1=r"(?<=mr |ms )\w.*\n?(?=\s?s\/o|\s?d\/o|\s?father\'s)"
    pattern2=r"(?<=miss )\w.*\n?(?=s\/o|d\/o|father\'s)"
    pattern3=r"(?<=mrs )\w.*\n?(?=s\/o|d\/o|father\'s)"
    pattern4=r"((?<=that))\s?\n?\w.*\n?(?=s\/d\/o)|(((?<=that))\s?\n?\w.*\n?(?=\s?[sd][\/i]o)(?=\ss[\/i]o))|((?<=that))\s?\n?\w.*\n?(?=\s?daughter|\s?son|\s?fathers)"
    pattern5=r"(?<=name )\w.*"
    if re.search(pattern1, text):
#         return re.search(pattern1, text).group(0)
        name = re.search(pattern1, text).group(0)
    elif re.search(pattern2, text):
        name = re.search(pattern2, text).group(0)
#         return re.search(pattern2, text).group(0)
    elif re.search(pattern3, text):
        name = re.search(pattern3, text).group(0)
#         return re.search(pattern3, text).group(0)
    elif re.search(pattern4, text):
        name = re.search(pattern4, text).group(0)
#         return re.search(pattern4, text).group(0)
    elif re.search(pattern5, text):
        name = re.search(pattern5, text).group(0)
#         return re.search(pattern5, text).group(0)
    else:
        return "None"
    if name!='':
        return do_proper(name)           # Calling method to format the candiate name

def search_father(text):                 # Method for Extracting Candiate Father Name.
    fname=''
        # Possible Patterns for Extracting Candiate Father Name
    pattern1=r"(?<=fathers name)\s?\n?\w.*((?= surname|\n*surname)|(\w.*))"
    pattern2=r"(?<=[sd][\/i]o\s)\w.*((?= surname|\n*surname)|(\w.*))"
    pattern3=r"(?<=f[\/i]name)\s?\n?\w.*((?= surname|\n*surname)|(\w.*))"
    pattern4=r"(?<={son of)\s?\n?\w.*((?= surname|\n*surname)|(\w.*))|(?<=daughter of)\s?\n?\w.*((?= surname|\n*surname)|(\w.*))"
    if re.search(pattern1, text):
        fname=re.search(pattern1, text).group(0)
#         return re.search(pattern1, text).group(0)
    elif re.search(pattern2, text):
        fname=re.search(pattern2, text).group(0)
#         return re.search(pattern2, text).group(0)
    elif re.search(pattern3, text):
        fname=re.search(pattern3, text).group(0)
#         return re.search(pattern3, text).group(0)
    elif re.search(pattern4, text):
        fname=re.search(pattern4, text).group(0)
#         return re.search(pattern4, text).group(0)
    else:
        return "None"
    if fname!='':
        return do_proper(fname)           # Calling method to format the candiate father name
    
    
def search_surname(text):                 # Method for Extracting Candiate Surname.
        # Pattern fo Extracting Surname...
    pattern=r"(?<=surname\n|surname )\w+"
    if re.search(pattern, text):
        return re.search(pattern, text).group(0).title() # Returns the Surname with title case...
    else:
        return "None"
    
def search_group(text):                   # Method for Extracting Candiate Grouo
        # Creating list for the verification of the Groups...
    lists=['preengineering','premedical','commerce', 'computer science','social sciences']
    temp=''
        # Possible patters for Extracting group
    pattern1=r'(preengineering|premedical|commerce|computer science|social sciences)'
    pattern2=r'(?<=group\n|group )\w*'
    if re.search(pattern1, text):
        temp = re.search(pattern1, text).group(0)  
#         return re.search(pattern1, text).group(0).title()
    if re.search(pattern2, text):
        temp = re.search(pattern2, text).group(0)
#         return re.search(pattern2, text).group(0).title()
    
    if temp in lists:
        return temp.title()              # Return the group with title case 
    else:
        return "None"
    
        

def search_certificate(text):            # Method for Extracting Candiate Certificate Name.
        # Pattern for Certificate
    pattern=r'higher secondary certificate'
    if re.search(pattern, text):
        return re.search(pattern, text).group(0).title() # Matches and returns the certificate with title case.
    else:
        return "None"
    

def search_seatno(text):                # Method for Extracting Candiate Seat No.
        # Possible Patterns for Extracting Seat Number...
    pattern1=r"(?<=seat no |seat no\n)\d*"
    pattern2=r"(?<=roll no |roll no\n)\d*"
    if re.search(pattern1, text):
        return re.search(pattern1, text).group(0)  # Matches and returns the seat number..
    elif re.search(pattern2, text):
        return re.search(pattern2, text).group(0)  # Matches and returns the seat number..
    else:
        return "None"
    
    
def search_grade(text):               # Method for Extracting Candiate Grade.   
        # Possible Patterns for Extracting Grade...
    pattern1 = r"(?<=grade\n|grade )([a][1]?|[b-f])(?!\w)"
    pattern2 = r"(?<=placed in\n|placed in )([a][1]?|[b-f])((?=grade)|(?!\w))"
    if re.search(pattern1, text):
        return re.search(pattern1, text).group(0).title() # Matches and returns the Grade..
    elif re.search(pattern2, text):
        return re.search(pattern2, text).group(0).title() # Matches and returns the Grade...
    else:
        return "None"
        
def search_year(text):              # Method for Extracting Candiate Passing.   
        # Possible Patterns for Extracting Passing Year...
    year_pattern=r"(?<=examination\n|examination )\d{4}"    
    if re.search(year_pattern, text):
        return re.search(year_pattern, text).group(0)     # Matches and returns the Year..
    else:
        return "None"
        
def search_board(text):             # Method for Extracting Candiate Board.   
        # Possible Patterns for Extracting Board Name...
    pattern=r'(board.*\n?.*(sindh|punjab|balochistan|peshawar|sukkur))'
    if re.search(pattern, text):
        return do_proper(re.search(pattern, text).group(0))  # Matches and returns the Board Name..
    else:
        return "None"
    
def search_dob(text):               # Method for Extracting Candiate Date of Birth.   
        # Possible Patterns for Extracting Date of Birth...
    temp_pattern=r'(date of birth(.*)\n*.*\d{4})'
    pattern=r"\d{2}[\/-]\d{2}[\/-]\d{4}"
    temp=''
    if re.search(temp_pattern, text):
        temp=re.search(temp_pattern, text).group(0)
        if re.search(pattern, temp):
            return re.search(pattern, temp).group(0)           # Matches and returns Date of Birth..
        else:
            return "None"
    else:
        return "None"


app = Flask(__name__)

app.config['UPLOAD_FOLDER']='/users/pardeeprassani/Documents/WebAPI_Intermediate/'

@app.route('/upload')
def upload_file():
    return render_template('upload.html')


#url = '/users/pardeeprassani/Documents/HandwrittenTextDetection/InterMarksheets/'
@app.route('/uploader', methods = ['GET', 'POST'])
def uploadd_file():
    if request.method == 'GET':
        f = request.args.get('file')
        #f.save(secure_filename(f.filename))
       
        
        #fileName=os.path.join(f.filename)

        #fileUrl = url+fileName

        Text = detect_document(f)        # Calling method for Detecting Text
    
        name=search_name(Text)                  # Calling method and storing value of candiate name in name
        fname=search_father(Text)               # Calling method and storing value of candiate father name in fname
        surname=search_surname(Text)            # Calling method and storing value of surname in surname
        certificate=search_certificate(Text)    # Calling method and storing value of certificate name in certificate
        group=search_group(Text)                # Calling method and storing value of Group in group
        seatno=search_seatno(Text)              # Calling method and storing value of Seat Number in seatno
        year=search_year(Text)                  # Calling method and storing value of Passing Year in year
        board=search_board(Text)                # Calling method and storing value of Board Name in board 
        grade=search_grade(Text)                # Calling method and storing value of Grade in grade
        
    details = [
        {'name': str(name),
         'fathername': str(fname),
         'surname': str(surname),
         'certificate': str(certificate),
         'group': str(group),
         'seatNo': str(seatno),
         'year': str(year),
         'board': str(board),
         'grade': str(grade)}
    ]

    return jsonify(details)
    #return img

if __name__ == '__main__':
    app.run(debug = True)