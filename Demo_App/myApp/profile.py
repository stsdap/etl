from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for,session
)
from werkzeug.exceptions import abort

from myApp.auth import login_required
from myApp.db import get_db

bp = Blueprint('dashboard', __name__)

@bp.route('/')
def user():
    return render_template('base.html')

@bp.route('/Profile/<string:name>')
def index(name):
    db = get_db()
    person = db.execute(
        'SELECT * FROM user WHERE username =?',(name,)
    ).fetchone()
    object={'username':person['username'], 'password':person['password'], 'email':person['email']}
    return render_template('dashboard/index.html',object=object, person=person)
    
def get_user(username, check_user=True):
    person = get_db().execute(
        'SELECT * FROM user WHERE username = ?', (username,)
    ).fetchone()

    if person is None:
        abort(404, "User {0} doesn't exist.".format(username))

    if check_user and username != person['username']:
        abort(403)

    return person

@bp.route('/<string:username>/update', methods=('GET', 'POST'))
def update(username):
    #person=get_user(username)    
    person = get_db().execute(
        'SELECT * FROM user WHERE username = ?', (username,)
    ).fetchone()
    
    if request.method == 'POST':
        name = request.form['username']
        password = request.form['password']
        mail = request.form['email']
        error = None
        try:
            db = get_db()
            db.execute(
                'UPDATE user SET username = ?, password = ?, email = ?'
                ' WHERE  username = ?' ,(name, password, mail, username)
            )
            db.commit()
            session.clear()
            session['username'] = name
            return redirect(url_for('dashboard.index', name=name))
            #return redirect(url_for('auth.logout'))
            #return redirect(url_for('dashboard.index', name=name))
            #object={'username':name,'password':password, 'email':mail}
            #return render_template('dashboard/index.html',object=object)
        except:
            error = 'User {0} already exists. Kindly Try Something Else. Thanks!'.format(name)
            flash(error)
            return redirect(url_for('dashboard.index', name=username))

    return render_template('dashboard/update.html', person=person)
