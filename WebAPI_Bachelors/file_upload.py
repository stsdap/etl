from flask import Flask, render_template, request, jsonify
from werkzeug import secure_filename
import io
import re
import os
from google.cloud import vision
from PIL import Image

os.environ["GOOGLE_APPLICATION_CREDENTIALS"]="/users/pardeeprassani/Documents/HandwrittenTextDetection/cosmic-tenure-233207-dff701184033.json"
os.environ["FLASK_ENV"] ="development"
os.environ["FLASK_APP"] = "file_upload.py"

def detect_document_text(path):
    """Detects document features in an image."""
    from google.cloud import vision
    client = vision.ImageAnnotatorClient()

    with io.open(path, 'rb') as image_file:
        content = image_file.read()

    image = vision.types.Image(content=content)

    response = client.document_text_detection(image=image)
    
    text = ""
    
    for page in response.full_text_annotation.pages:
        for block in page.blocks:
            for paragraph in block.paragraphs:
                for word in paragraph.words:
                    word_text = ''.join([
                        symbol.text for symbol in word.symbols
                    ])
                    text = text + word_text+" "
#                    print('Text Detected: {}'.format(
#                        word_text))
    return text


# In[56]:


def extract_name(text):
    ''' this method detect name in image\'s text and purify it
        so that exact name in image is returned'''
    # name can contain these titles (remove when not required)
    titles = ["Ms.","Mr.","Ms","Mr","Mrs.","Mrs","Miss","Miss.","whereas","conferred","upon"]
    
    # pattern can detect these symbols (remove when unnecessary)
    punctuations = [".",",","/","\\","%"]

    # pattern (Mr\s?\.?|Ms\s?\.?|Mrs\s?\.?) (\s?\w+\s?)* ((D\/O)|(S\/O)|(%))
    
    # pattern to detect in text
    pattern = r'((whereas)|(conferred upon)|(mr\s?\.?\s?\/?\s?|ms\s?\.?\s?\/?\s?|mrs\s?\.?\s?\/?\s?|miss\s?\.?\s?\/?\s?)) (\s?\.?\s?\w+\s?)* ((d\s?\/\s?o)|(s\s?\/\s?o?)|(%))'
#    pattern = re.compile(pattern)

    name = ""

    match = re.search(pattern,text)          # Search for pattern in text to be matched
    if match is not None:                    # IF pattern is matched
        matched = match.group(0)             # THEN fetch all matched text
        nameList = matched.split(" ")        # AND separate them with spaces i.e. make a list
        i=0                                  # iterator (i.e. index) to remove unnecessary text from matched pattern
        while i<len(nameList):               # while index is in valid range
            print(nameList)                  # for satisfication view text while being modified (not necessary you can remove it)
            if len(nameList[i])<=2 or nameList[i] in titles or nameList[i] in punctuations:
                print("removing : "+nameList[i])
                nameList.remove(nameList[i])
            else:
                i+=1
        
        name = " ".join(nameList)
    else:
        print ("name not found")
    return name


# In[77]:


def extract_fathername(text):
    # keywords to exclude from detected pattern
    keywords=['%','s / o','d / o','has','in']

    # pattern to detect fathername
    pattern = r'((d\s?\/\s?o)|(s\s?\/\s?o?)|(%)) (\s?\w+\s?){1,3}'
    
    fathername=""
    
    match = re.search(pattern,text)          # Search for pattern in text to be matched
    if match is not None:                    # IF pattern is matched
        matched = match.group(0)             # THEN fetch all matched text
        nameList = matched.split(" ")        # AND separate them with spaces i.e. make a list
        i=0                                  # iterator (i.e. index) to remove unnecessary text from matched pattern
        while i<len(nameList):               # while index is in valid range
            print(nameList)                  # for satisfication view text while being modified (not necessary you can remove it)
            if len(nameList[i])<=2 or nameList[i] in keywords:
                print("removing : "+nameList[i])
                nameList.remove(nameList[i])
            else:
                i+=1
        
        fathername = " ".join(nameList)
    else:
        print("father name not found")
    return fathername


# In[78]:


def extract_year(text):
    months =['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec',
            'january','february','march','april','june','july','august','september','october','november','december']
    
    pattern = r'\w{3,9} (\d?\d)?\s?\,?\.?\s?\d\d\d\d'        #pattern to detect in text

    year = ""
    match = re.search(pattern,text)                          # search pattern in text
    print(match)
    if match is not None:
        year = match.group(0)
        if "year" in year:
            year = year.replace('year',"")
    else:
        print ("year not found")
    return year


# In[79]:


def extract_position(text):
    # pattern to detect secured grade/division from degree's text
    pattern = r'secured (\w+ \w+)|(\s?\w+\s?(division?\/?\s?|class?\s?\/?\s?division?)|(\W\s?\w{1,2}\s?\W\s?grade))'#(secured (\w+ \w+))|
    pattern = re.compile(pattern)
    
    position = ""
    
    match = re.search(pattern,text)
    if match is not None:
        position = match.group(0)
        if "secured" in position:
            position = position.replace("secured","")
    else:
        print("position not found")
    return position


# In[80]:


def extract_degree_name(text):
    # keywords to remove from degree
    key_words = ['has','is']
    
    pattern = r'degree of bachelor of(\s?\w+\s?){1,2}(\(?(\s?\w+\s?){1,3}\s?\){1}?)?'#'degree of bachelor(\s?\w+\s?)*\s\((\s?\w+\s?)*\s?\)'
    pattern = re.compile(pattern)
    degree = ""
    match = re.search(pattern,text)
    if match is not None:
        degree = match.group(0)
        degreeList = degree.split(" ")
        degreeList = degreeList[2:]
        degree = " ".join(degreeList)
        for keyword in key_words:
            if keyword in degree:
                degree=degree.replace(keyword,"")
    else:
        print("degree not found")
    return degree


# In[81]:


def extract_university_name(text):
    # pattern to detect from the text
    pattern = r'((\s?\w+\s?\W?){1,3} university (of(\W?(\s?\w+\s?)\W){1,3})?(\W?(\s?\w+\s?)\W){1})|((\s\w+\s?)\s?institut[e|(ion)]\s?of\s?(\s?\w+\s?){1,2})'
    
    university_name=""
    
    match = re.search(pattern,text)
    if match is not None:
        university_name=match.group(0)
    else:
        print("university name not found")
    return university_name


app = Flask(__name__)

app.config['UPLOAD_FOLDER']='/users/pardeeprassani/Documents/DemoApi/'

@app.route('/upload')
def upload_file():
    return render_template('upload.html')

@app.route('/uploader', methods = ['GET', 'POST'])
def uploadd_file():
    if request.method == 'GET':
        f = request.args.get('file')
        #f.save(secure_filename(f.filename))
       
        #fileName= secure_filename(f.filename)
        #fileName=os.path.join(f.filename)
        text = detect_document_text(f)
        text = text.lower()
        
        name = extract_name(text).strip()
        fathername = extract_fathername(text).strip()
        year = extract_year(text).strip()
        position = extract_position(text).strip()
        degree = extract_degree_name(text).strip()
        university = extract_university_name(text).strip()
        
    details = [
        {'name': str(name),
         'fathername': str(fathername),
         'year': str(year),
         'position': str(position),
         'degree': str(degree),
         'university': str(university)}
    ]

    return jsonify(details)
    #return img

if __name__ == '__main__':
    app.run(debug = True)