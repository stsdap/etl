
#import Flask class using flask module

from flask import Flask             # Flask's object will be our WSGI application
from flask import render_template   # to return html template file
from flask import url_for           # to link to other files using path....
import sqlite3                      # for database
from flask import request           # for requesting from server....
from flask import json,jsonify
# create object of Flask class
application = Flask(__name__) # first argument is the name of the application's module or package. 

#json = FlaskJSON(application)
# this sets secret key in order to defend from attacks.
application.config['SECRET_KEY']='Dayanand'

# use route decorator to tell Flask what URL should tigger our function.

# to view this type in browser http://localhost:5000/home OR simply http://localhost:5000
@application.route("/")
@application.route("/home")

# define function
def home():
    return render_template('home.html', title="Home Page")

# to view this type in browser http://localhost:5000/about
# OR http:127.0.0.1:5000/addrecord

@application.route("/addrecord", methods=['POST','GET'])
def addrecord():
    return render_template('addrecord.html',title="Adding Record")

# method to add record to the database
@application.route("/newRecord",methods=["POST","GET"])
def newRecord():
    msg="No action performed.."
    if request.method == 'POST':
        connection = sqlite3.connect("database.db")
        try:
            object ={"name":request.form['name'],"fathername":request.form['fathername'],"email":request.form['email']}
#            object =" {\"name\":"+request.form['name']+",\"fathername\":"+request.form['fathername']+",\"email\":"+request.form['email']+"}"
#            object = json.loads(object)
            cursor  = connection.cursor()
            cursor.execute("INSERT INTO CANDIDATES (name,fathername,email) VALUES (?,?,?)",(object['name'],object['fathername'],object['email']))
            connection.commit()
            msg = "Record successfully added"
        except e:
            connection.rollback()
            msg = "error in insert operation"      
        finally:
            connection.commit()
            connection.close()
            if msg is None:
                return render_template('addrecord.html')
            else:
                return render_template("addrecord.html",msg=msg, object=object)

# Method to update data to the server
@application.route("/updateRecord",methods=["POST","GET"])
def updateRecord():
    if request.method =="POST":
        try:
            fathername = request.form['fathername']
            email = request.form['email']

            with sqlite3.connect('database.db') as connection:
                connection.execute("UPDATE CANDIDATES SET fathername="+fathername+" email="+email+" WHERE name=")

            msg="Record updated successfully!..."
        except:
            connection.rollback()
            msg="Error in updating Record!!!"
        finally:
            connection.commit()
            connection.close()
            if msg is None:
                return render_template('updaterecord.html')
            else:
                return render_template('updaterecord.html',msg=msg)



# Method to view Record i.e. fetch from database and show it in web
@application.route("/viewrecord", methods=["POST","GET"])
def viewrecord():
    connection = sqlite3.connect('database.db')

    connection.row_factory = sqlite3.Row

    cursor = connection.cursor()

    cursor.execute("SELECT * FROM CANDIDATES")

    rows = cursor.fetchall()

    objects = []

    for row in rows:
        obj = {"name": row['name'], "fathername": row['fathername'], "email": row['email']}    
        objects.append(obj)

    return render_template('viewrecord.html',title="Viewing Record", objects=objects)

# Method to update Record....
@application.route("/updaterecord")
def updaterecord():
    return render_template('updaterecord.html',title="Updating Record")

@application.route('/searchForUpdate', methods=["POST","GET"])
def searchForUpdate():
    if request.method=="POST":
        msg="No action performed!..."
        rows=None
        object=[]

        try:
            name = request.form['name']

            connection = sqlite3.connect('database.db')
            connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute("SELECT * FROM CANDIDATES WHERE name LIKE '"+name+"'")
            rows = cursor.fetchall()
            for row in rows:
                obj = {'name':row['name'],'fathername':row['fathername'],'email':row['email']}
                object.append(obj)
            msg="Record found!..."
        except:
            connection.rollback()
            msg="Record not found!!!"
        finally:
            connection.commit()
            connection.close()
            if msg is None:
                return render_template('searchForUpdate.html')
            else:
                return render_template('searchForUpdate.html',msg=msg, object=object)

@application.route('/update', methods=["POST","GET"])
def update():
    if request.method =="POST":
        msg=""
        connection = sqlite3.connect('database.db')
        try:
            
            connection.execute("UPDATE CANDIDATES SET fathername='"+request.form['fathername']+"', email='"+request.form['email']+"' WHERE name LIKE '"+request.form['name']+"'")

            msg="Record updated successfully!..."
        except:
            connection.rollback()
            msg="Error in updating Record!!!"
        finally:
            connection.commit()
            connection.close()
            if msg is None:
                return render_template('updaterecord.html')
            else:
                return render_template('updaterecord.html',msg=msg)


# Method to delete the record from the database.
@application.route("/deleterecord", methods=["POST","GET"])
def deleterecord():
    return render_template('deleterecord.html',title="Deleting Record")

@application.route('/searchForDelete', methods=["POST","GET"])
def searchForDelete():
    if request.method=="POST":
        msg="No action performed!..."
        rows=None
        object = {}

        try:
            name = request.form['name']

            connection = sqlite3.connect('database.db')
            connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            cursor.execute("SELECT * FROM CANDIDATES WHERE name LIKE '"+name+"'")
            rows = cursor.fetchall()
            for row in rows:
                object = {'name':row['name'],'fathername':row['fathername'],'email':row['email']}
#                object.append(obj)
            msg="Record found!..."
        except:
            connection.rollback()
            msg="Record not found!!!"
        finally:
            connection.commit()
            connection.close()
            if msg is None:
                return render_template('searchForDelete.html')
            else:
                return render_template('searchForDelete.html',msg=msg,rows=rows, object=object)

@application.route('/delete', methods=["POST","GET"])
def delete():
    if request.method =="POST":
        msg=""
        connection = sqlite3.connect('database.db')
        try:
            
            connection.execute("DELETE FROM CANDIDATES WHERE name LIKE '"+request.form['name']+"'")

            msg="Record deleted successfully!..."
        except:
            connection.rollback()
            msg="Error in deleting Record!!!"
        finally:
            connection.commit()
            connection.close()
            if msg is None:
                return render_template('deleterecord.html')
            else:
                return render_template('deleterecord.html',msg=msg)



# if we run this code using Python then we have to add following lines to our code.
if __name__=="__main__":
    application.run(DEBUG=True)
