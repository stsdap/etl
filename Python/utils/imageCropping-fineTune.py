
# coding: utf-8

# In[2]:


import cv2
import sys
import glob 
import os

for image in glob.glob('Forms/*.jpg'): #directory of images to crop
    imageName=os.path.basename(image) 
    image = cv2.imread(image)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    faceCascade = cv2.CascadeClassifier(cv2.data.haarcascades + "haarcascade_frontalface_default.xml")
    faces = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.3,
        minNeighbors=3,
        minSize=(30, 30)
    )
    
    iw=312 #ideal wirdth
    ih=400 #ideal height
    
    for (x, y, w, h) in faces:
                
        xf=round(iw-w)
        xf=round(xf/2)
        x1=int(x-xf)
        x2=x1+iw
        
        yf=round(iw-w)
        yf=round(yf/2)
        y1=int(y-yf)
        y2=y1+ih
        
        roi_image = image[y1:y2, x1:x2]
        cv2.imwrite('fineTuneImages/'+imageName, roi_image) #directory to save cropped image

