#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
from google.cloud import vision
import io
import argparse
import json
import re
import csv
import pandas as pd 
import mysql.connector
from mysql.connector import Error
from datetime import datetime
from dateutil.parser import parse
from word2number import w2n
import numpy
import xml.etree.ElementTree as ET

os.environ["GOOGLE_APPLICATION_CREDENTIALS"]="/users/pardeeprassani/Documents/HandwrittenTextDetection/cosmic-tenure-233207-dff701184033.json"


# In[2]:


def detect_confidence(path):
    """Detects line confidence in an image."""

    client = vision.ImageAnnotatorClient()

    with io.open(path, 'rb') as image_file:
        content = image_file.read()

    image = vision.types.Image(content=content)

    response = client.document_text_detection(image=image)    
    
    breaks = vision.enums.TextAnnotation.DetectedBreak.BreakType
    
    df = pd.DataFrame(columns=['line','confidence'])

    for page in response.full_text_annotation.pages:
        for block in page.blocks:
            for paragraph in block.paragraphs:
                
                line = ""
                conf = 0
                count = 0
                
                for word in paragraph.words:
                    conf +=word.confidence
                    count+=1
                    for symbol in word.symbols:
                        line += symbol.text 
                        line = line.casefold()
                        line = line.replace('(','')
                        line = line.replace(')','')
                        line = line.replace(':','')
                        line = line.replace('.','')
                        
                        if symbol.property.detected_break.type == breaks.SPACE:
                            line += ' '
                        if symbol.property.detected_break.type == breaks.EOL_SURE_SPACE:
                            line += ' '
                            df = df.append({'line' : line , 'confidence' : conf/count} , ignore_index=True)
                            line = ''
                        if symbol.property.detected_break.type == breaks.LINE_BREAK:
                            df = df.append({'line' : line , 'confidence' : conf/count} , ignore_index=True)
                            line = ''
                            
    return df


# In[3]:


def setContact(string):
    if type(string) is pd.core.series.Series:
        string = string.str.replace('s','5')
        string = string.str.replace('o','0')
        string = string.str.replace('i','1')
        string = string.str.replace('b','6')
        string = string.str.replace('u','4')
        string = string.str.replace('g','2')
        string = string.str.replace('l','1')
    else:
        string = string.replace('s','5')
        string = string.replace('o','0')
        string = string.replace('i','1')
        string = string.replace('b','6')
        string = string.replace('u','4')
        string = string.replace('g','2')
        string = string.replace('l','1')
    return string


# In[4]:


def countInContact(string):
    count=0
    if type(string) is pd.core.series.Series:
        count+= string.str.count('s')
        count+= string.str.count('o')
        count+= string.str.count('i')
        count+= string.str.count('b')
        count+= string.str.count('u')
        count+= string.str.count('g')
        count+= string.str.count('l')
    else:
        count+= string.count('s')
        count+= string.count('o')
        count+= string.count('i')
        count+= string.count('b')
        count+= string.count('u')
        count+= string.count('g')
        count+= string.count('l')
    return count


# In[6]:


def setCNIC(string):
    if type(string) is pd.core.series.Series:
        string = string.str.replace('\s+','')
        string = string.str.replace('s','5')
        string = string.str.replace('o','0')
        string = string.str.replace('i','1')
        string = string.str.replace('b','6')
        string = string.str.replace('u','4')
        string = string.str.replace('g','2')
        string = string.str.replace('l','1')
    else:
        string = re.sub('\s+','', string)
        string = string.replace('s','5')
        string = string.replace('o','0')
        string = string.replace('i','1')
        string = string.replace('b','6')
        string = string.replace('u','4')
        string = string.replace('g','2')
        string = string.replace('l','1')
    return string


# In[7]:


def countInCNIC(string):
    counter=0
    if type(string) is pd.core.series.Series:
        counter+=string.str.count('\s+')
        counter+=string.str.count('s')
        counter+=string.str.count('o')
        counter+=string.str.count('i')
        counter+=string.str.count('b')
        counter+=string.str.count('u')
        counter+=string.str.count('g')
        counter+=string.str.count('l')
    else:
        counter+=len(re.findall('\s+', string))
        counter+=string.count('s')
        counter+=string.count('o')
        counter+=string.count('i')
        counter+=string.count('b')
        counter+=string.count('u')
        counter+=string.count('g')
        counter+=string.count('l')
    return counter


# In[8]:


def setDOB(string):
    def is_date(string, fuzzy=False):
    
        #Return whether the string can be interpreted as a date.
        try: 
            parse(string, fuzzy=fuzzy)
            return True

        except ValueError:
            return False
    
    if type(string) is pd.core.series.Series:
        string = string.str.replace('\s+','')
        string = string.str.replace('o','0')
        string = string.str.replace('i','1')
        string = string.str.replace('%','8')
        
        string = string.str.replace('s','5')
        string = string.str.replace('b','6')
        string = string.str.replace('u','4')
        string = string.str.replace('g','2')
        string = string.str.replace('l','1')
        
        for index, value in string.iteritems():
            if is_date(value):
                string[index] = parse(value).date()
            else:
                string[index]="NaN"
    else:
        string = re.sub('\s+','', string)
        string = string.replace('o','0')
        string = string.replace('i','1')
        string = string.replace('%','8')
        
        string = string.replace('s','5')
        string = string.replace('b','6')
        string = string.replace('u','4')
        string = string.replace('g','2')
        string = string.replace('l','1')
        
        if is_date(string):
            string = parse(string).date()
        else:
            string="NaN"
    return string


# In[ ]:





# In[9]:


def countInDOB(string):
    count=0
    
    if type(string) is pd.core.series.Series:
        count+=string.str.count('\s+')
        count+=string.str.count('o')
        count+=string.str.count('i')
        count+=string.str.count('%')
        
        count+=string.str.count('s')
        count+=string.str.count('b')
        count+=string.str.count('u')
        count+=string.str.count('g')
        count+=string.str.count('l')
        
    else:
        count+=len(re.findall('\s+', string))
        count+=string.count('o')
        count+=string.count('i')
        count+=string.count('%')
        
        count+=string.count('s',)
        count+=string.count('b')
        count+=string.count('u')
        count+=string.count('g')
        count+=string.count('l')
        
    return count


# In[ ]:





# In[10]:


def setAge(string):
    from word2number import w2n
    def is_wordNum(string):
        #Return whether the string can be interpreted as a number.
        try: 
            w2n.word_to_num(string)
            return True

        except ValueError:
            return False

    if type(string) is pd.core.series.Series:
        for index, value in string.iteritems():
            value = str(value)
            if value.isdigit():
                value = int(value)
                if value > 100:
                    string[index]=int(value/10)
                else:
                    string[index]=value
            elif is_wordNum(value):
                string[index] = w2n.word_to_num(value)
            else:
                string[index]= "None"
    else:
        if string.isdigit():
            string = int(string)
            if string > 100:
                string=int(string/10)
            else:
                string=string
        elif is_wordNum(string):
            string = w2n.word_to_num(string)
        else:
            string = "None"
    return string


# In[11]:


def countInAge(string):
    from word2number import w2n
    def is_wordNum(string):
        #Return whether the string can be interpreted as a number.
        try: 
            w2n.word_to_num(string)
            return True
        except ValueError:
            return False
    count=0
    if type(string) is pd.core.series.Series:
        for index, value in string.iteritems():
            value = str(value)
            if value.isdigit():
                value = int(value)
                if value > 100:
                    count+=1
                    #string[index]=int(value/10)
                else:
                    string[index]=value
            elif is_wordNum(value):
                count+=1
    else:
        if string.isdigit():
            string = int(string)
            if string > 100:
                count+=1
        elif is_wordNum(string):
            count+=1
    return count


# In[12]:


#for name, hname, fname, domicile
def countInName(string):
    count =0
    if type(string) is pd.core.series.Series:
        count+=string.str.count('\d+')
        count+=string.str.count('([^\s\w]|_)+')
        count+= string.str.count('^\s]([ ]{2,})[^\s]')
        count+= string.str.count('i{2,}')
    else:
        count+=len(re.findall('\d+',string))
        count+=len(re.findall('([^\s\w]|_)+',string))
        count+=len(re.findall('[^\s]([ ]{2,})[^\s]',string))
        count+=len(re.findall('i{2,}',string))
    return count


# In[13]:


#for name, hname, fname, domicile
def setName(string):
    if type(string) is pd.core.series.Series:
        string = string.str.replace('\d+','')
        string = string.str.replace('([^\s\w]|_)+','')
        string = string.str.replace('[^\s]([ ]{2,})[^\s]',' ')
        string = string.str.replace('i{2,}', 'i')
    else:
        string = re.sub('\d+','', string)
        string = re.sub('([^\s\w]|_)+','', string)
        #string = re.sub('\s+',' ',string)
        string = re.sub('[^\s]([ ]{2,})[^\s]',' ',string)
        string = re.sub('i{2,}', 'i',string)
    return string


# In[ ]:





# In[14]:


data = pd.read_csv('/users/pardeeprassani/Documents/HandwrittenTextDetection/Forms/Cropped/Testing/candidateDetails.csv')


# In[15]:


data


# In[4]:


xmlString = """<data>
    <name>
        <origin></origin>
        <originscore></originscore>
        <final></final>
        <corrections></corrections>
    </name>
    <hname>
        <origin></origin>
        <originscore></originscore>
        <final></final>
        <corrections></corrections>
    </hname>
    <fname>
        <origin></origin>
        <originscore></originscore>
        <final></final>
        <corrections></corrections>
    </fname>
    <cnic>
        <origin></origin>
        <originscore></originscore>
        <final></final>
        <corrections></corrections>
    </cnic>
    <gender>
        <origin></origin>
        <originscore></originscore>
        <final></final>
        <corrections></corrections>
    </gender>
    <age>
        <origin></origin>
        <originscore></originscore>
        <final></final>
        <corrections></corrections>
    </age>
    <dateofbirth>
        <origin></origin>
        <originscore></originscore>
        <final></final>
        <corrections></corrections>
    </dateofbirth>
    <domicile>
        <origin></origin>
        <originscore></originscore>
        <final></final>
        <corrections></corrections>
    </domicile>
    <contact>
        <origin></origin>
        <originscore></originscore>
        <final></final>
        <corrections></corrections>
    </contact>
    <address>
        <origin></origin>
        <originscore></originscore>
        <final></final>
        <corrections></corrections>
    </address>
</data>
"""

root = ET.fromstring(xmlString)


# In[17]:


pd.options.display.max_colwidth=100


# In[53]:


directory = "/users/pardeeprassani/Documents/HandwrittenTextDetection/Forms/Cropped/Testing"
imagesPath=[]
for root, dirs, files in os.walk(directory):
    for file in files:
        if file.endswith('.jpg'):
             imagesPath.append(os.path.join(root, file))
            


# In[55]:


dfName = pd.DataFrame(columns=['fieldName','origin','confidence', 'final','corrections'])
dfFName = pd.DataFrame(columns=['fieldName','origin','confidence', 'final','corrections'])
dfHName = pd.DataFrame(columns=['fieldName','origin','confidence', 'final','corrections'])
dfCnic = pd.DataFrame(columns=['fieldName','origin','confidence', 'final','corrections'])
dfGender = pd.DataFrame(columns=['fieldName','origin','confidence', 'final','corrections'])
dfAge = pd.DataFrame(columns=['fieldName','origin','confidence', 'final','corrections'])
dfDob = pd.DataFrame(columns=['fieldName','origin','confidence', 'final','corrections'])
dfDomicile = pd.DataFrame(columns=['fieldName','origin','confidence', 'final','corrections'])
dfContact = pd.DataFrame(columns=['fieldName','origin','confidence', 'final','corrections'])
dfLog = pd.DataFrame(columns=['fieldName','origin','confidence', 'final','corrections'])

st=[]
for inputImage in imagesPath:
    lines = detect_confidence(inputImage)
    st.append(lines)
    for index, value in data.name.iteritems():
        for index2, value2 in lines.line.iteritems():
            if value in value2:
                dfName = dfName.append({'fieldName':"name",'origin' : value , 'confidence' : lines.confidence[index2], 'final': setName(value),'corrections': countInName(value)} , ignore_index=True)     

    for index, value in data.fname.iteritems():
        for index2, value2 in lines.line.iteritems():
            if value in value2:
                dfFName = dfFName.append({'fieldName':'fname','origin' : value , 'confidence' : lines.confidence[index2], 'final': setName(value),'corrections': countInName(value)} , ignore_index=True)   
    
    for index, value in data.hname.iteritems():
        for index2, value2 in lines.line.iteritems():
            if value in value2:
                dfHName = dfHName.append({'fieldName':'hname','origin' : value , 'confidence' : lines.confidence[index2], 'final': setName(value),'corrections': countInName(value)} , ignore_index=True)   
                
    for index, value in data.cnic.iteritems():
        for index2, value2 in lines.line.iteritems():
            if value in value2:
                dfCnic = dfCnic.append({'fieldName':'cnic','origin' : value , 'confidence' : lines.confidence[index2], 'final': setCNIC(value),'corrections': countInCNIC(value)} , ignore_index=True) 
    
    for index, value in data.age.iteritems():
        for index2, value2 in lines.line.iteritems():
            if value in value2 and 'age in years' in value2:
                dfAge = dfAge.append({'fieldName':'age','origin' : value , 'confidence' : lines.confidence[index2], 'final': setAge(value),'corrections': countInAge(value)} , ignore_index=True)     
    
    for index, value in data.gender.iteritems():
        for index2, value2 in lines.line.iteritems():
            if value in value2 and "gender" in value2:
                dfGender = dfGender.append({'fieldName':'gender','origin' : value , 'confidence' : lines.confidence[index2], 'final': setName(value),'corrections': countInName(value)} , ignore_index=True) 
                
    for index, value in data.dateOfBirth.iteritems():
        for index2, value2 in lines.line.iteritems():
            if value in value2:
                dfDob = dfDob.append({'fieldName':'dateOfBirth','origin' : value , 'confidence' : lines.confidence[index2], 'final': setDOB(value),'corrections': countInDOB(value)} , ignore_index=True)   
                
    for index, value in data.domicile.iteritems():
        for index2, value2 in lines.line.iteritems():
            if value in value2 and "domicile district" in value2:
                dfDomicile = dfDomicile.append({'fieldName':'domicile','origin' : value , 'confidence' : lines.confidence[index2], 'final': setName(value),'corrections': countInName(value)} , ignore_index=True)   
                
    for index, value in data.contact.iteritems():
        for index2, value2 in lines.line.iteritems():
            if value in value2:
                dfContact = dfContact.append({'fieldName':'contact','origin' : value , 'confidence' : lines.confidence[index2], 'final': setContact(value),'corrections': countInContact(value)} , ignore_index=True)     
        
dfAge = dfAge.drop_duplicates().reset_index(drop=True)
dfGender = dfGender.drop_duplicates().reset_index(drop=True)
                
dfLog = dfLog.append([dfName,dfFName, dfHName,dfCnic,dfContact, dfDob,dfAge,dfDomicile,dfGender], ignore_index=True)


# In[57]:


dupLog=dfLog


# In[ ]:





# In[311]:


pd.set_option('display.max_rows', len(st[2]))
st[2]


# In[ ]:




