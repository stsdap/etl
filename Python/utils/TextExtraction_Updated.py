#!/usr/bin/env python
# coding: utf-8

# In[2]:


import os
from google.cloud import vision
import io
import argparse
import json
import re
import csv
import pandas as pd 
import mysql.connector
from mysql.connector import Error
from datetime import datetime
from dateutil.parser import parse
from word2number import w2n
import numpy

os.environ["GOOGLE_APPLICATION_CREDENTIALS"]="/users/pardeeprassani/Documents/HandwrittenTextDetection/cosmic-tenure-233207-dff701184033.json"


# In[3]:


#for name, hname, fname, domicile
def setName(string):
    if type(string) is pd.core.series.Series:
        string = string.str.replace('\d+','')
        string = string.str.replace('([^\s\w]|_)+','')
        string = string.str.replace('\s+',' ')
        string = string.str.replace('i{2,}', 'i')
    else:
        string = string.replace('\d+','')
        string = string.replace('([^\s\w]|_)+','')
        string = string.replace('\s+',' ')
        string = string.replace('i{2,}', 'i')
    return string


# In[4]:


#highlights names that have more than specified spaces
def highlightSpaces(string, nSpaces):
    if type(string) is pd.core.series.Series:
        for index, value in string.iteritems():
            value = value.lstrip()
            value = value.rstrip()
            if value.count(' ') > nSpaces:
                string[index]=value.upper()
    else:
        string = string.lstrip()
        string = string.rstrip()
        if string.count(' ') > nSpaces:
            string = string.upper()
    return string


# In[5]:


def is_wordNum(string):
    """
    Return whether the string can be interpreted as a number.
    """
    try: 
        w2n.word_to_num(string)
        return True

    except ValueError:
        return False


# In[49]:


def setAge(string):
    if type(string) is pd.core.series.Series:
        for index, value in string.iteritems():
            value = str(value)
            if value.isdigit():
                value = int(value)
                if value > 100:
                    string[index]=int(value/10)
                else:
                    string[index]=value
            elif is_wordNum(value):
                string[index] = w2n.word_to_num(value)
            else:
                string[index]= "None"
    else:
        if string.isdigit():
            string = int(string)
            if string > 100:
                string=int(string/10)
            else:
                string=string
        elif is_wordNum(string):
            string = w2n.word_to_num(string)
        else:
            string = "None"
    return string


# In[7]:


def is_date(string, fuzzy=False):
    """
    Return whether the string can be interpreted as a date.
    """
    try: 
        parse(string, fuzzy=fuzzy)
        return True

    except ValueError:
        return False


# In[54]:


def setDOB(string):
    if type(string) is pd.core.series.Series:
        string = string.str.replace('\s+','')
        string = string.str.replace('o','0')
        string = string.str.replace('i','1')
        string = string.str.replace('%','8')
        
        string = string.str.replace('s','5')
        string = string.str.replace('b','6')
        string = string.str.replace('u','4')
        string = string.str.replace('g','2')
        string = string.str.replace('l','1')
        
        for index, value in string.iteritems():
            if is_date(value):
                string[index] = parse(value).date()
            else:
                string[index]="NaN"
    else:
        string = string.replace('\s+','')
        string = string.replace('o','0')
        string = string.replace('i','1')
        string = string.replace('%','8')
        
        string = string.str.replace('s','5')
        string = string.str.replace('b','6')
        string = string.str.replace('u','4')
        string = string.str.replace('g','2')
        string = string.str.replace('l','1')
        
        for index, value in string.iteritems():
            if is_date(value):
                string[index] = parse(value).date()
            else:
                data.dateOfBirth[index]=numpy.nan
    return string


# In[9]:


def setCNIC(string):
    if type(string) is pd.core.series.Series:
        string = string.str.replace('\s+','')
        string = string.str.replace('s','5')
        string = string.str.replace('o','0')
        string = string.str.replace('i','1')
        string = string.str.replace('b','6')
        string = string.str.replace('u','4')
        string = string.str.replace('g','2')
        string = string.str.replace('l','1')
    else:
        string = string.replace('\s+','')
        string = string.replace('s','5')
        string = string.replace('o','0')
        string = string.replace('i','1')
        string = string.replace('b','6')
        string = string.replace('u','4')
        string = string.replace('g','2')
        string = string.replace('l','1')
    return string


# In[10]:


def splitContact(df, col):
    if type(df) is pd.core.frame.DataFrame:
        if type(col) is pd.core.series.Series:
            df.insert(8,"primaryNum",col.str.replace('([^\w]|_)+','').str[:11])
            df.insert(9, "alternateNum", col.str.replace('([^\w]|_)+','').str[11:])
            #df=df.drop(['contact'],axis=1)
            #del col
        else:
            print("Error: "+col+" is not a Series")
            return False
    else:
        print("Error: "+df+" is not a DataFrame")
        return False
    return True


# In[11]:


def removeLines(inputFile):
    os.system('convert \( ' + inputFile + ' -alpha off \)     \( -clone 0 -morphology close rectangle:1x50 -negate \)     \( -clone 0 -morphology close rectangle:50x1 -negate \)     \( -clone 1 -clone 2 -evaluate-sequence add  \)     -delete 1,2     -compose plus -composite '+inputFile)
    return True


# In[ ]:





# In[12]:


def detect_document(path):
    """Detects document features in an image."""

    client = vision.ImageAnnotatorClient()

    with io.open(path, 'rb') as image_file:
        content = image_file.read()

    image = vision.types.Image(content=content)

    response = client.document_text_detection(image=image)
    
    text = response.full_text_annotation.text
    text = text.casefold()
    text = text.replace('(','')
    text = text.replace(')','')
    text = text.replace(':','')
    text = text.replace('.','')
    
    return text


# In[13]:


directory = "/users/pardeeprassani/Documents/HandwrittenTextDetection/Forms/Cropped/Testing"
imagesPath=[]
for root, dirs, files in os.walk(directory):
    for file in files:
        if file.endswith('.jpg'):
             imagesPath.append(os.path.join(root, file))
            


# In[35]:


name = ''
address = ''
fname = ''
hname = ''
cnic = ''
gender = ''
age = ''
dob = ''
domicile = ''
contact = ''

namePattern = '\nname (.*)'
fathersnamePattern = '\nfather\'s name (.*)'
husbandnamePattern = '\nhusband\'s name (.*)'
CNICPattern = '\ncomputerized nic no (.*)'
genderPattern = 'gender (\w+)'
agePattern = 'age in years (.\w+)'
dobPattern = 'date of birth (.*)'
domicilePattern = '\ndomicile district (.\w+)'
contactPattern = 'contact no ([ \w-]+)\n.*'
addressPattern = '\npostal address (.*\n.*)'

csvFile = open(directory+'/candidateDetails.csv', 'a', newline='')

# create the csv writer object
csvwriter = csv.writer(csvFile)

csvwriter.writerow(["name", "fname", "hname" ,"cnic", "gender", "age", "dateOfBirth","domicile", "contact", "address"])

strs = []

for inputImage in imagesPath:
    removeLines(inputImage)
    string = detect_document(inputImage)
    strs.append(string)

    if re.search(fathersnamePattern,string):
        fname = re.search(fathersnamePattern,string).group(1)
    else: 
        fname = "None"

    if re.search(namePattern,string):
        name = re.search(namePattern,string).group(1)
    else: 
        name = "None"
        
    if re.search(husbandnamePattern,string):
        hname = re.search(husbandnamePattern,string).group(1)
    else: 
        hname = "None"

    if re.search(CNICPattern,string):
        cnic = re.search(CNICPattern,string).group(1)
    else: 
        cnic = "None"

    if re.search(genderPattern,string):
        gender = re.search(genderPattern,string).group(1)
    else: 
        gender = "None"

    if re.search(agePattern,string):
        age = re.search(agePattern,string).group(1)
    else: 
        age = "None"

    if re.search(dobPattern,string):
        dob = re.search(dobPattern,string).group(1)
    else: 
        dob = "None"

    if re.search(contactPattern,string):
        contact = re.search(contactPattern,string).group(1)
    else:
        contact = "None"
        
    if re.search(domicilePattern,string):
        domicile = re.search(domicilePattern,string).group(1)
    else: 
        domicile = "None"
    
    if re.search(addressPattern,string):
        address = re.search(addressPattern,string).group(1).replace('\n',' ')
    else: 
        address = "None"

    dictObj =  '[{"Name": "'+name+'", "Father\'sName": "'+fname+'","Husband\'sName": "'+hname+'", "CNIC": "'+cnic+'","Gender": "'+gender+'","Age": "'+age+'", "DateOfBirth": "'+dob+'","Domicile": "'+domicile+'","Contact": "'+contact+'","Address": "'+address+'"}]'
    
    #convert = json.dumps(dictObj, indent=' ')
    # convert into JSON:
    candidateDetails = json.loads(dictObj)
    
    for column in candidateDetails:
        csvwriter.writerow(column.values())
csvFile.close()


# In[ ]:





# In[36]:


data = pd.read_csv('/users/pardeeprassani/Documents/HandwrittenTextDetection/Forms/Cropped/Testing/candidateDetails.csv')


# In[37]:


data


# In[32]:


data.name=setName(data.name)
data.name


# In[37]:


data.fname = setName(data.fname)
data.fname


# In[53]:


data.cnic=setCNIC(data.cnic)
data.cnic


# In[52]:


setAge(data.age)
data.age


# In[44]:


data.dateOfBirth=setDOB(data.dateOfBirth)
data.dateOfBirth


# In[ ]:





# In[ ]:





# In[ ]:





# In[110]:


pd.options.display.max_colwidth=100

