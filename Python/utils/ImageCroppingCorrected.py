#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# importing packages
import numpy as np
import cv2 as cv
import math
from scipy import ndimage

# image location
imageLocation = '/home/dayanand/Documents/DAP/SD-74 && SD-83/Forms270/Form'

# multiple cascades: https://github.com/Itseez/opencv/tree/master/data/haarcascades

# three cascades for face, eyes, and nose

#https://github.com/Itseez/opencv/blob/master/data/haarcascades/haarcascade_frontalface_default.xml
face_cascade = cv.CascadeClassifier('haarcascade_frontalface_default.xml')

#https://github.com/Itseez/opencv/blob/master/data/haarcascades/haarcascade_eye.xml
eye_cascade = cv.CascadeClassifier('haarcascade_eye.xml')

#https://github.com/opencv/opencv_contrib/blob/master/modules/face/data/cascades/haarcascade_mcs_nose.xml
nose_cascade = cv.CascadeClassifier('haarcascade_mcs_nose.xml')

# Method to crop passport-size photo from form
def cropImage(image):
    gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
    face = face_cascade.detectMultiScale(gray, scaleFactor=1.3, minNeighbors=3, minSize=(30, 30))    
    face = face_cascade.detectMultiScale(
        gray,
        scaleFactor=1.3,
        minNeighbors=3,
        minSize=(30, 30)
    )
    iw=312 #ideal wirdth
    ih=400 #ideal height

    eyeNo=1
    
    for (x, y, w, h) in face:            
        xf=round(iw-w)
        xf=round(xf/2)
        x1=int(x-xf)
        x2=x1+iw
        
        yf=round(iw-w)
        yf=round(yf/2)
        y1=int(y-yf)
        y2=y1+ih
        roi_gray = gray[y:y+h, x:x+w]
        roi_color = image[y:y+h, x:x+w]
        eyes = eye_cascade.detectMultiScale(roi_gray)
        for (ex,ey,ew,eh) in eyes:
            radius = int(round((ew + eh)*0.25))
            eyeNo+=1        
        if eyeNo>2:
            croppedImage = image[y1:y2, x1:x2]
            cv.imwrite('/home/dayanand/Documents/DAP/SD-74 && SD-83/CorrectForms/CroppedImages/Form'+str(im)+"-Pic.jpg", croppedImage) #directory to save corrected image
            break
    
# Method to perform rotation on image
def performImageRotation(image):
    #print("Image rotating")
    return ndimage.rotate(image,90)    


def verifyFaceDetection(image):
    #print("verifying face detection")
    gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
    face = face_cascade.detectMultiScale(gray, 1.3, 5)
    nose = nose_cascade.detectMultiScale(gray, 1.3, 5)
    noseRestriction = 1
    for (nx,ny,nw,nh) in nose:
        if noseRestriction != 1:
            break
        noseRestriction+=1
    eyeNo=1
    for (x,y,w,h) in face:
        roi_gray = gray[y:y+h, x:x+w]
        eyes = eye_cascade.detectMultiScale(roi_gray)
        # Here eyeNo is used to detect either left or right eye

        for (ex,ey,ew,eh) in eyes:
            if eyeNo>2:
                break
            eyeNo+=1

    if eyeNo >= 3 and noseRestriction >= 2:
        return True 
    else:
        return False
    
def processImage(image,im):
    for i in range(3):
        if verifyFaceDetection(image):
            break
        else:
            image = performImageRotation(image)                
    cv.imwrite('/home/dayanand/Documents/DAP/SD-74 && SD-83/CorrectForms/Form'+str(im)+".jpg", image) #directory to save corrected image
    cropImage(image)
    print("image saved...")    

    
for im in range(1,11):
    # read image
    image = cv.imread(imageLocation+str(im)+".jpg",-1)
    print(str(im))
    processImage(image,im)


# In[ ]:




