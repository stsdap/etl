#!/usr/bin/env python
# coding: utf-8

# In[1]:


# importing packages
import io
import cv2
import re
import os
import sys
import glob 
from google.cloud import vision

os.environ["GOOGLE_APPLICATION_CREDENTIALS"]="/home/dayanand/Documents/DAP/dayasham-a2d3b3295c88.json"


# In[2]:


'''Remove lines --code to remove lines from the marksheet'''
# inputFile is for imput image location
def removeLines(inputFile):
    os.system('convert \( ' + inputFile + ' -alpha off \)     \( -clone 0 -morphology close rectangle:1x50 -negate \)     \( -clone 0 -morphology close rectangle:50x1 -negate \)     \( -clone 1 -clone 2 -evaluate-sequence add  \)     -delete 1,2     -compose plus -composite  /home/dayanand/Documents/DAP/SD-95/marksheet-result.jpg')
    return True


# In[3]:


def detect_document_text(path):
    """Detects document features in an image."""
    from google.cloud import vision
    client = vision.ImageAnnotatorClient()

    with io.open(path, 'rb') as image_file:
        content = image_file.read()

    image = vision.types.Image(content=content)

    response = client.document_text_detection(image=image)
    
    text = ""
    
    for page in response.full_text_annotation.pages:
        for block in page.blocks:
            for paragraph in block.paragraphs:
                for word in paragraph.words:
                    word_text = ''.join([
                        symbol.text for symbol in word.symbols
                    ])
                    text = text + word_text+" "
#                    print('Text Detected: {}'.format(
#                        word_text))
    text = text.lower()
    return text


# In[4]:


import pandas as pd

d = {'Subjects':[],
     'Thry-9th Alloc':[],
     'Pract-9th Alloc':[],
     'Total-9th Alloc':[],
     'Thry-10th Alloc':[],
     'Pract-10th Alloc':[],
     'Total-10th Alloc':[],
     'Thry-9th Obt':[],
     'Pract-9th Obt':[],
     'Thry-10th Obt':[],
     'Pract-10 Obt':[],
     'Total':[],
     'In words':[]
    }
df = pd.DataFrame(d)
df


# In[5]:


#\d\s?\.\s?(\w+\s?([a-z]+)?)(\((\s?\w+\s?){1,2}\))?\s?(\s?\W?\s?[\d\d\d]?\s?\W?\s?){1,}(\s?\D+\s?){1,3}
# Final Pattern: \d\s?\.\s?(\w+\s?([a-z]+)?)(\((\s?\w+\s?){1,2}\))?\s?(\s?\W?\s?[\d\d\d]?\s?\W?\s?){1,}(\s?\D+\s?){1,3}

def getSubjects(text):
    pattern = r'\d\s?\.\s?(\w+\s?([a-z]+)?)(\((\s?\w+\s?){1,2}\))?'
    subjects=[]
    match = re.findall(pattern,text)
    for m in match:
        l = list(m)
        l=l[:-3]
        s ="".join(l)
        subjects.append(s)
#    print(match)
#    print(subjects)
    return subjects
        


# In[6]:


def getMarks(text):
    pattern = r'\d\s?\.\s?(\w+\s?([a-z]+)?)(\((\s?\w+\s?){1,2}\))?(\s?(\s?\W?\s?[\d\d\d]?\s?\W?\s?){1,}(\s?\D+\s?){1,3})'
    Thry9thallocatedMarks=[]  #1
    Pract9thallocatedMarks=[]  #2
    total9thallocatedMarks=[]  #3
    Thry10thallocatedMarks=[]  #4
    Pract10thallocatedMarks=[]  #5
    total10thallocatedMarks=[]  #6
    Thry9thobtainedMarks=[]    #7
    Pract9thobtainedMarks=[]   #8
    Thry10thobtainedMarks=[]   #9
    Pract10thobtainedMarks=[]  #10
    totalMarks=[]             #11
    inWords=[]              #12 onwards
    match = re.findall(pattern,text)
    for m in match:
        s= ''.join(m[4:])
        s.rstrip('')
        l = s.split()
        Thry9thallocatedMarks.append(l[0])  #1
        Pract9thallocatedMarks.append(l[1])  #2
        total9thallocatedMarks.append(l[2])  #3
        Thry10thallocatedMarks.append(l[3])  #4
        Pract10thallocatedMarks.append(l[4])  #5
        total10thallocatedMarks.append(l[5])  #6
        Thry9thobtainedMarks.append(l[6])    #7
        Pract9thobtainedMarks.append(l[7])   #8
        Thry10thobtainedMarks.append(l[8])   #9
        Pract10thobtainedMarks.append(l[9])  #10
        totalMarks.append(l[10])             #11
        word = ' '.join(l[11:])
        inWords.append(word)             #12 onwards
        
#        print(l)
#    print(match)
    data=[Thry9thallocatedMarks, Pract9thallocatedMarks,total9thallocatedMarks, 
    Thry10thallocatedMarks, Pract10thallocatedMarks, 
    total10thallocatedMarks, Thry9thobtainedMarks, 
    Pract9thobtainedMarks, Thry10thobtainedMarks, 
    Pract10thobtainedMarks, totalMarks, inWords]
    return data
    


# In[7]:


text ="board of intermediate and secondary education конат detailed marks certificate secondary school certificate examination s . no . kb enrol no . 043 - kht / upkt - 10 session 2012 ( annual ) science group roll no . 60007 name . jamal hussain father ' s name . muhammad jalil regular student of : govt higher secondary school usterzai payan kohat marks alloted marks obtained in figures subjects 9th 10th 9th 10th in words thry pract total thry pract total thry pract thry pract | total 1 . english - 75 75 - 150 55 - 52 - 107 one hundred seven 2 . urdu - 75 75 - 150 49 - 51 - 100 one hundred only 3 . islamiyat ( comp ) - 75 - - 75 32 - - 32 thirty - two 4 . pakistan studies - - 75 75 - - 59 - 59 fifty - nine 5 . mathematics - 75 75 - 150 51 - 56 - 107 one hundred seven 6 . physics 65 10 75 65 10 150 48 10 52 10 120 one hundred twenty only 7 . chemistry 65 10 75 65 10 150 46 10 37 10 103 one hundred three 8 . biology 65 10 75 65 10 150 44 10 37 10 101 one hundred one total 1050 total seven hundred twenty nine only remarks date of birth : 26 - 03 - 1996 ( twenty - six march ninteen hundred & ninety - six . ) note : - errors / omissions excepted result date : 12 - 06 - 2012 issue date : 12 - 06 - 2012 computer cell , bise , kohat ( naveed \" ) checked by : controller of examinations bise , kohat"
getMarks(text)


# In[8]:


text ="board of intermediate and secondary education конат detailed marks certificate secondary school certificate examination s . no . kb enrol no . 043 - kht / upkt - 10 session 2012 ( annual ) science group roll no . 60007 name . jamal hussain father ' s name . muhammad jalil regular student of : govt higher secondary school usterzai payan kohat marks alloted marks obtained in figures subjects 9th 10th 9th 10th in words thry pract total thry pract total thry pract thry pract | total 1 . english - 75 75 - 150 55 - 52 - 107 one hundred seven 2 . urdu - 75 75 - 150 49 - 51 - 100 one hundred only 3 . islamiyat ( comp ) - 75 - - 75 32 - - 32 thirty - two 4 . pakistan studies - - 75 75 - - 59 - 59 fifty - nine 5 . mathematics - 75 75 - 150 51 - 56 - 107 one hundred seven 6 . physics 65 10 75 65 10 150 48 10 52 10 120 one hundred twenty only 7 . chemistry 65 10 75 65 10 150 46 10 37 10 103 one hundred three 8 . biology 65 10 75 65 10 150 44 10 37 10 101 one hundred one total 1050 total seven hundred twenty nine only remarks date of birth : 26 - 03 - 1996 ( twenty - six march ninteen hundred & ninety - six . ) note : - errors / omissions excepted result date : 12 - 06 - 2012 issue date : 12 - 06 - 2012 computer cell , bise , kohat ( naveed \" ) checked by : controller of examinations bise , kohat"
getSubjects(text)


# In[9]:


import pandas as pd
path='/home/dayanand/Documents/DAP/SD-95/marksheet-result.jpg'
text = detect_document_text(path)
subjects = getSubjects(text)
data = getMarks(text)
d = {'Subjects': subjects,
     'Thry-9th Alloc':data[0],
     'Pract-9th Alloc':data[1],
     'Total-9th Alloc':data[2],
     'Thry-10th Alloc':data[3],
     'Pract-10th Alloc':data[4],
     'Total-10th Alloc':data[5],
     'Thry-9th Obt':data[6],
     'Pract-9th Obt':data[7],
     'Thry-10th Obt':data[8],
     'Pract-10 Obt':data[9],
     'Total':data[10],
     'In words':data[11]
    }
df = pd.DataFrame(d)
df


# In[ ]:




