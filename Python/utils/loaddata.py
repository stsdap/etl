
# coding: utf-8
print(spark.version)
akey_raw = spark.read.csv('./answerkey.csv', header=True, inferSchema=True, ignoreLeadingWhiteSpace=True,                      ignoreTrailingWhiteSpace=True)
akey = akey_raw.select('QNO','Answer').dropna().withColumnRenamed('Answer','ANSWER')
akey.count()
from pyspark.sql.functions import *
from pyspark.sql.types import *
akey = akey.withColumn('TESTID',lit('SBOR001'))
akey.show()
akey.printSchema()
url = 'jdbc:mysql://ibatech001:3306/stsdap'
candidate_raw = spark.read.csv('./candidates.csv', header=True, inferSchema=True, ignoreLeadingWhiteSpace=True,                      ignoreTrailingWhiteSpace=True)
candidate_clean = candidate_raw.select('SEATNO','NAME','CNIC','TESTCENTRE','TOTALMARKS').dropna()
candidate_clean.withColumn('GENDER',expr('CNIC'))

def get_gender(cnic):
    gcode = int(float(cnic[-1]))
    if gcode % 2 > 0:
        return 'M'
    else:
        return 'F'
    
gender = udf(lambda x: get_gender(x), StringType())
candidate_gender = candidate_clean.withColumn('GENDER',gender(candidate_clean.CNIC))

def get_division(cnic):
    return int(float(cnic[1]))
    
division = udf(lambda x: get_division(x), IntegerType())
candidate_division = candidate_gender.withColumn('DIVISION',division(candidate_gender.CNIC))

def get_district(cnic):
    return int(float(cnic[2]))
    
district = udf(lambda x: get_district(x), IntegerType())
candidate_district = candidate_division.withColumn('DISTRICT',district(candidate_division.CNIC))

def get_taluka(cnic):
    return int(float(cnic[3]))
    
taluka = udf(lambda x: get_taluka(x), IntegerType())
candidate_taluka = candidate_district.withColumn('TALUKA',taluka(candidate_district.CNIC))
candidate_taluka.show()
candidate = candidate_taluka.withColumn('TESTID',lit('SBOR001'))
candidate.show()
candidate.printSchema()
candidate.write.jdbc(url = url, table='CANDIDATE', properties={"user":"stsdapowner","password":"areYouWorthy2019?"})

result_raw = spark.read.csv('./karachi.csv', header=True, inferSchema=True, ignoreLeadingWhiteSpace=True,                      ignoreTrailingWhiteSpace=True)
result_raw.show()
result = result_raw.select('SEATNO',
 'Q1',
 'Q2',
 'Q3',
 'Q4',
 'Q5',
 'Q6',
 'Q7',
 'Q8',
 'Q9',
 'Q10',
 'Q11',
 'Q12',
 'Q13',
 'Q14',
 'Q15',
 'Q16',
 'Q17',
 'Q18',
 'Q19',
 'Q20',
 'Q21',
 'Q22',
 'Q23',
 'Q24',
 'Q25',
 'Q26',
 'Q27',
 'Q28',
 'Q29',
 'Q30',
 'Q31',
 'Q32',
 'Q33',
 'Q34',
 'Q35',
 'Q36',
 'Q37',
 'Q38',
 'Q39',
 'Q40',
 'Q41',
 'Q42',
 'Q43',
 'Q44',
 'Q45',
 'Q46',
 'Q47',
 'Q48',
 'Q49',
 'Q50',
 'Q51',
 'Q52',
 'Q53',
 'Q54',
 'Q55',
 'Q56',
 'Q57',
 'Q58',
 'Q59',
 'Q60',
 'Q61',
 'Q62',
 'Q63',
 'Q64',
 'Q65',
 'Q66',
 'Q67',
 'Q68',
 'Q69',
 'Q70',
 'Q71',
 'Q72',
 'Q73',
 'Q74',
 'Q75',
 'Q76',
 'Q77',
 'Q78',
 'Q79',
 'Q80',
 'Q81',
 'Q82',
 'Q83',
 'Q84',
 'Q85',
 'Q86',
 'Q87',
 'Q88',
 'Q89',
 'Q90',
 'Q91',
 'Q92',
 'Q93',
 'Q94',
 'Q95',
 'Q96',
 'Q97',
 'Q98',
 'Q99',
 'Q100')


result.show()
result.printSchema()
res = result.collect()
row = res[0]
row.asDict().keys()
rowlist = []
for row in res:
    for i in range(100):
        tup = (row.asDict()['SEATNO'],i+1,row.asDict()['Q'+str(i+1)])
        rowlist.append(tup)

rowlist[950]
candidate_answers = spark.createDataFrame(rowlist,['SEATNO','QNO','ANSWER']).withColumn('TESTID',lit('SBOR001'))
candidate_answers.show()
candidate_answers.count()

candidate_answers.write.jdbc(url = url, table='CANDIDATE_ANSWER', mode='append',properties={"user":"stsdapowner","password":"areYouWorthy2019?"})

